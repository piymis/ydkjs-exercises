/*
 * Program to calculate the price of phone purchase
 */

const TAX_RATE = 0.06;
const PHONE_PRICE = 100;
const ACCESSORY_PRICE = 10;
const SPENDING_THRESHOLD = 150;

var purchaseAmount = 0, bankBalance = 500;

function calculateTax(amount) {
    return amount * TAX_RATE;
}

function formatAmount(amount) {
    return "Rs" + amount.toFixed(2);
}

while (purchaseAmount < bankBalance) {
    purchaseAmount += PHONE_PRICE;
    if (purchaseAmount < SPENDING_THRESHOLD) {
        purchaseAmount += ACCESSORY_PRICE;
    }
}
 
purchaseAmount += calculateTax(purchaseAmount);
console.log(
        "Total purchase amount is: " + formatAmount(purchaseAmount)
        );
if (purchaseAmount > bankBalance) {
    console.log(
            "You cannot make this purchase"
            );
}

